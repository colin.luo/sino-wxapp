window._alert = alert;
window.alert = function(text) {

	if (typeof text !== 'string') {
		text = JSON.stringify(text);
	}

	var $dialog = $('<dialog class="modal"><div class="modal-title">温馨提示</div>' +
		'<div class="modal-body">'+ text +'</div>' +
		'<div class="modal-footer"><a href="javascript:void(0)" class="btn btn-block" rel="modal:close">知道了</a></div>' +
	'</dialog>');

	$dialog.on('click', '[rel="modal:close"]', function() {
		setTimeout(function() {
			$dialog.remove();
			$dialog = null;
		}, 200);
	});

	$dialog.modal({
		closeExisting: false
	});
}



jQuery(function($) {

	// ---- 全部课程下拉菜单: --------------------------------------------------------
	$('#btn-all-course-menu').click(function(event) {
		event.preventDefault();

		var url = this.href,
				menuid = 'all-course-menu',
				$menu = $('#'+ menuid);

		if ($menu.length > 0) {
			$menu.removeClass('fadeOut').addClass('fadeIn');
			$menu.show();
		} else {
			$menu = $('<div id="'+ menuid +'" class="all-course-menu loading"></div>').appendTo('body');
			$.get(url).then(function(html){
				$menu.html(html).removeClass('loading');
			});
			$menu.on('click', function(event) {

				var target = event.target;

				if (target.nodeName === 'A') {
					var href = target.getAttribute('href');

					if (!href.match(/^(javascript|\#)/)) {
						event.preventDefault();
						$menu.removeClass('fadeIn').addClass('fadeOut');
						setTimeout(function() {
							document.location = href;
						}, 200);
					} else {

						if ($(target).parent().hasClass('opened')) {
							$(target).parent()
								.removeClass('opened').addClass('closed');
						} else {
							$(target).parent()
								.removeClass('closed').addClass('opened')
								.siblings('.opened')
								.removeClass('opened').addClass('closed');
						}
					}
				} else {
					$menu.removeClass('fadeIn').addClass('fadeOut');
					setTimeout(function() {
						$menu.hide();
					}, 400);
				}

			});
		}
	});

	// ---- 课程详情的评论排序:  --------------------------------------------------------------------
	var commentsSortorTicket;
	var commentIscrollInited = false;
	$('.comments-sortor')
		.on('click', '>a', function(event) {
			var $sorter = $(this).parent();

			if ($sorter.hasClass('active')) {
				$sorter.removeClass('active');
			} else {
				$sorter.addClass('active');

				clearTimeout(commentsSortorTicket);
				commentsSortorTicket = setTimeout(function() {
					$(document).on('click.comments-sortor', function(event) {
						$sorter.removeClass('active');
						$(document).off('.comments-sortor');
					});
				}, 200);
			}
		})
		.on('click', 'li>a', function(event) {
			event.preventDefault();

			$('.comemnt-list').empty();

			window.COMMENT_SORT_TYPE = $(this).attr('data-sort');
			window.PULLER.setPageNo(1);

			$(this).addClass('cur')
				.parents('li:first').siblings('li').find('.cur').removeClass('cur');
		});

	// --- 课程详情底部评论的发送按钮: ------------------------------------------------------------——

	$('body').on('click', '.btn-send-comment', function(event) {
		event.preventDefault();

		var $editor = $('.comments-add .content-editor');
		var comment = $editor.html();
		var url = $(this).attr('href');

		if (!comment) {
			alert('请先输入评论。');
		} else {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'JSON',
				data: {comment: encodeURIComponent(comment)},
				success: function() {
					var activeLink = $('.comments-sortor li a.cur');
					activeLink = activeLink.length >0 ? activeLink : $('.comments-sortor li a:first');
					activeLink.trigger('click');
					$editor.html('');
				}
			});
		}
	});

	// --- 评论“赞”按钮： ------------------------------------------------------------------------
	$('body').on('click', '.btn-praise', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');
		var cancelUrl = $btn.attr('cancel-url');

		if (!$btn.hasClass('disabled')) {
			// 增加赞
			$.getJSON(url).then(function(result) {
				result = result || {};
				if (result.errorCode == 0) {
					var count = parseInt($btn.find('>span').text() || '');
					$btn.find('>span').text(count+1);
					$btn.addClass('disabled');
				} else {
					//alert('您已经赞过。');
				}
			});
		} else {
			// 取消赞
			$.getJSON(cancelUrl).then(function(result) {
				result = result || {};
				if (result.errorCode == 0) {
					var count = parseInt($btn.find('>span').text() || '');
					$btn.find('>span').text(Math.max(0, count-1));
					$btn.removeClass('disabled');
				} else {
					//alert('您已经赞过。');
				}
			});
		}
	});

	// --- 评论：添加回复 ------------------------------------------------------------------------
	$('body').on('click', '.btn-reply', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		var $modal = $('#modalReply').modal();

		$modal
		.off('click.reply')
		.on('click.reply', 'a.btn-send-reply', function(event) {
			var reply = encodeURIComponent($modal.find('.reply-editor').html());

			if (!reply) {
				alert('请输入回复内容.');
			} else {

				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'JSON',
					data: {comment: reply},
					success: function(result) {
						$modal.find('.reply-editor').html('');
						$.modal.close();
						alert('回复成功');
					},
					error: function() {
						$.modal.close();
					}
				});
			}
		});

	});

	// --- 评价按钮: ------------------------------------------------------------------------

	$('body').on('click', '.btn-star', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var id = $btn.attr('data-id');
		var url = $btn.attr('href') || $btn.attr('data-url');

		if ($btn.hasClass('disabled')) {
			$('#modalStarSuccess').modal();
			return;
		}

		var $modal = $('#modalStar').modal();

		$modal.off('.star')

		// 评价星星
		.on('click.star', 'a.btn-star-item', function(event) {
			event.preventDefault();

			var $star = $(this);
			$star.parent().attr('data-star', $star.index() + 1);

		})

		// 评价标签
		.on('click.star', 'a.btn-tag', function(event) {
			$modal.find('input').val($(this).text());
		})

		// 评价确定按钮
		.on('click.star', 'a.btn-send-star', function(event) {

			var $stars = $modal.find('.star-plugin');
			var data = {
				star: parseInt($stars.attr('data-star') || '0'),
				comment: encodeURIComponent($modal.find('input').val())
			};

			if (!data.comment) {
				alert('请输入评价内容或者选择标签');
			} else {

				data.star = data.star || 5; // 默认5星好评；

				$.ajax({
					url: url,
					data: data,
					dataType: 'JSON',
					type: 'POST',
					success: function(result) {
						result = result || {};

						if (result.errorCode == 0) {
							var $count = $('.comment-count');
							var $target = $count.filter('[data-id="'+ id +'"]');

							if ($target.length > 0) {
								$count = $target;
							}
							var number = parseInt($count.text() || 0);
							$count.html(number + 1);

							$.modal.close();
							$modal.find('.star-plugin').attr('data-star', 5);
							$('#modalStarSuccess').modal();

							$btn.addClass('disabled').text('已评价');
							$modal.find('input').val('');
						} else {
							alert(result.errorMassage || '服务器保存评论失败, 并且response[].errorMassage为空.');
						}
					}
				});
			}
		});

	});

	// ---- 报名按钮： ----------------------------------------------------------------------
	$('body').on('click', '.btn-join', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		if ($btn.hasClass('disabled')) {
			$('#modalJoinedAlready').modal();
		} else {
			$.getJSON(url).then(function(result) {
				result = result || {};

				if (result.errorCode == 0) {
					$('#modalJoinSuccess').modal();
					$btn.addClass('disabled').text('已预报名');
				} else {
					alert(result.errorMassage || '服务器保存失败, 并且response[].errorMassage为空.');
				}
			});
		}
	});
});
